from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

# Create your tests here.
class DaftarDataTest(TestCase):
    def test_landing_page_url_is_exist(self):
        response= Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_apakah_ada_button(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn('button', content)