from django.apps import AppConfig


class Tk1AppConfig(AppConfig):
    name = 'tk1app'
