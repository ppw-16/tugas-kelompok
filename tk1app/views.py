from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    return render(request,'index.html', {})

def error(request):
	return render(request,'404.html',{})
