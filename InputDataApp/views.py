from django.shortcuts import render,redirect
from django.http import Http404, HttpResponse
from .models import tempatWisata
from .forms import ContactForm
# Create your views here.
def inputdata(request):
	if request.method == "POST":
		form = ContactForm(request.POST)
		if form.is_valid():
			model_instance = tempatWisata(
				nama = form.data['nama'],
				tempat = form.data['tempat'],
				harga = form.data['harga'],
				musim = form.data['musim'],
				deskripsi = form.data['deskripsi'],
			# 	foto = form.data['foto']
			)
			model_instance.save()
			return redirect("/daftar/")
				
	else:
		form = ContactForm()
		

	argument={
		'form': form 
	}


	return render(request, "InputData.html",argument)	