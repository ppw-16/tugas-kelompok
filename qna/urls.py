from django.urls import path

from . import views

urlpatterns = [
    path('', views.index), 
    path('recentquestion/', views.index, name='recent'),
    # path('mostanswer/', views.most, name='most'),
    # path('noanswer/', views.noanswer, name='noanswer'),
]