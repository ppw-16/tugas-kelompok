from django.shortcuts import render, redirect

from .forms import QnA_Form, Answer_Form
from .models import QnA_Model, Answer_Model
# Create your views here.

def index(request):
	qnaform = QnA_Form(request.POST or None)
	answerform = Answer_Form(request.POST or None)

	if request.method == 'POST':
		if qnaform.is_valid():
			qnaform.save()

			return redirect('qna:recent')

		elif answerform.is_valid():
			answerform.save()

			return redirect('qna:recent')


	qnamodel = QnA_Model.objects.all()
	answermodel = Answer_Model.objects.all()

	context = {
		'data_form' : qnaform,
		'data_form2': answerform,
		'data_model' : qnamodel,
		'data_model2': answermodel,
	}

	return render(request,'qna/qna.html', context)


# def most(request):
# 	qnaform = QnA_Form(request.POST or None)

# 	if request.method == 'POST':
# 		if qnaform.is_valid():
# 			qnaform.save()

# 			return redirect('qna:recent')

# 	qnamodel = QnA_Model.objects.all()

# 	context = {
# 		'data_form' : qnaform,
# 		'data_model' : qnamodel,
# 	}
# 	return render(request,'qna/qna.html', context)

# def noanswer(request):
# 	qnaform = QnA_Form(request.POST or None)

# 	if request.method == 'POST':
# 		if qnaform.is_valid():
# 			qnaform.save()

# 			return redirect('qna:recent')

# 	qnamodel = QnA_Model.objects.all()

# 	context = {
# 		'data_form' : qnaform,
# 		'data_model' : qnamodel,
# 	}
# 	return render(request,'qna/qna.html', context)