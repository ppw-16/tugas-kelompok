from django.contrib import admin
from .models import QnA_Model, Answer_Model

admin.site.register(QnA_Model)
admin.site.register(Answer_Model)
# Register your models here.
