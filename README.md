[![pipeline status](https://gitlab.com/ppw-16/tugas-kelompok/badges/master/pipeline.svg)](https://gitlab.com/ppw-16/tugas-kelompok/commits/master)
[![coverage report](https://gitlab.com/ppw-16/tugas-kelompok/badges/master/coverage.svg)](https://gitlab.com/ppw-16/tugas-kelompok/commits/master)


PPW-E
Kelompok 16

Nama Anggota:
- Muhammad Alfi Syakir 	    (1806191364)
- Anisa Hasna Nabila 		(1806146865)
- Stephen Handiar Christian	(1806205703)
- Azkiya Hanna Rofifah 	    (1806191704)

Link HerokuApp:
https://ppw-16.herokuapp.com/

Aplikasi yang akan dibuat:
Aplikasi yang akan kami buat adalah aplikasi bertemakan pariwisata. Di dalam website tersebut berisikan tempat-tempat wisata yang ada di Indonesia. Daftar tempat wisata ini memiliki manfaat sebagai penampung atau wadah untuk menyimpan aset-aset berupa tempat wisata yang akan memudahkan masyarakat dan juga pelancong yang ingin berkunjung ke tempat tersebut untuk melihat kondisi serta informasi mengenai tempat tersebut.

Fitur di dalam aplikasi:
- (app: daftar) Filter Harga, Kategori, Daftar Tempat Wisata, Landing Page (Muhammad Alfi Syakir)
- (app: InputDataApp) Pendaftaran Tempat Wisata (Stephen Handiar Christian)
- (app: qna) QnA (Anisa Hasna Nabila)
- (app: detail) Detail Informasi, Komentar (Azkiya Hanna Rofifah)
