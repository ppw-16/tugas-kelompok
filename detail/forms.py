from django import forms
from django.db import models
from .models import Komentar
class form_komentar(forms.ModelForm):
    class Meta :
        model = Komentar
        fields = {"name","comment"}
        widgets = {
            'name' : forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder' : 'Type your name',
                }
            ),
            'comment' : forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder' : 'Give your comment here!',
                }
            ),
        }
        labels = {
            'name' : 'Name :  ',
            'comment' : 'Comment :  ',
        }
