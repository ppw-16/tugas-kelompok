from django.shortcuts import render,redirect
from .forms import form_komentar
from .models import Komentar
from InputDataApp.models import tempatWisata

def form(request,id):
    dictio={}
    data = Komentar.objects.all()
    form = form_komentar()

    if request.method == "POST":
        wisata = tempatWisata.objects.filter(id=id)[0]
        dictio['wisata'] = wisata
        form = form_komentar(request.POST or None)
        dictio['form']=form
        if form.is_valid():
            model = Komentar(name=form['name'].value(),comment=form['comment'].value())
            model.save()
            dictio['data'] = data
            return render(request,'detail.html',dictio)
    elif data.count !=0 :
        wisata = tempatWisata.objects.filter(id=id)[0]
        return render(request, 'detail.html', {'form': form,'data':data,'wisata':wisata})
    
    return render(request,'detail.html',{'form':form,'wisata':wisata})
