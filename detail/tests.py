from django.test import TestCase,Client
from .models import Komentar
from .forms import form_komentar
from .views import form

class DetailTest(TestCase):
    # def test_apakah_ada_text_box_buat_nama(self):
    #     c = Client()
    #     response = c.get('/detail/1/')
    #     content = response.content.decode('utf8')
    #     self.assertIn("text",content)
    #     self.assertIn("name",content)
    # def test_apakah_ada_text_box_buat_komentar(self):
    #     c = Client()
    #     response = c.get('/detail/1/')
    #     content = response.content.decode('utf8')
    #     self.assertIn("text",content)
    #     self.assertIn("comment",content)
    # def test_apakah_ada_button_add(self):
    #     c = Client()
    #     response = c.get('/detail/1/')
    #     content = response.content.decode('utf8')
    #     self.assertIn("<button",content)
    #     self.assertIn("Add",content)
    # def test_buat_model_Komentar(self, name = "testName",comment = "testComment"):
    #     return Komentar.objects.create(name=name,comment=comment)
    # def test_apakah_ada_model_Komentar(self):
    #     k = self.test_buat_model_Komentar()
    #     self.assertTrue(isinstance(k,Komentar))
    #     self.assertEqual(k.__unicode__(),k.name)
    # def test_apakah_teks_box_kosong_diterima_sebagai_inputan(self):
    #     c = Client()
    #     komentar = Komentar.objects.create(name='',comment='')
    #     data = {'name' : komentar.name, 'comment' : komentar.comment,}
    #     formKomentar = form_komentar(data=data)
    #     response = c.post('/detail/1/', data = {'name' : komentar.name, 'comment' : komentar.comment,})
    #     self.assertFalse(form.is_valid())
    # def test_apakah_input_valid_di_simpan_dalam_database(self):
    #     c = Client()
    #     komentar = Komentar(name="nameValid",comment="validComment")
    #     semua_isinya = komentar.objects.all()
    #     responsse = c.post('/detail/1/')
    #     self.assertEqual(semua_isinya.count(),1)
    def apakah_kalo_teks_box_nama_dan_komentar_diisi_muncul_ke_halaman_tersebut(self):
        c = Client()
        nama_yang_komentar = "namaSaya"
        komentarnya = "komentarSaya"
        response = c.post('/detail/1/',data = {'name':nama_yang_komentar,'comment' : komentarnya})
        content = response.content.decode('utf8')
        self.assertIn(nama_yang_komentar,content)
        self.asssertIn(komentarnya,content)